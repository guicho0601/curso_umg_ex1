package com.umg.curso.principal;

import com.umg.curso.clases.Deporte;
import com.umg.curso.clases.Deportista;
import com.umg.curso.clases.Especialista;
import com.umg.curso.clases.Ministerio;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ore on 28/06/17.
 */
public class ConsoleApp {

    public static void main(String[] args) throws Exception {

        Ministerio ministerio = new Ministerio();
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir) {

            System.out.println("1. Datos de Especialista asignado");
            System.out.println("2. Adicionar deporte");
            System.out.println("3. Agregar deportista");
            System.out.println("4. Listar deportes con balón");
            System.out.println("5. Desplegar promedio de edad");
            System.out.println("6. Desplegar datos del especialista");
            System.out.println("7. Desplegar todos los deportistas");
            System.out.println("0. Salir");

            System.out.println("Sistema del Ministerio de Deportes");

            try {
                System.out.println("Seleccione una de las opciones");
                opcion = sn.nextInt();

                switch (opcion) {
                    case 1:
                        System.out.println("Nombre:");
                        Especialista especialista = new Especialista();
                        sn.nextLine();
                        especialista.setNombre(sn.nextLine());
                        System.out.println("Años de experiencia:");
                        especialista.setAnnosExp(sn.nextInt());
                        ministerio.setEspecialista(especialista);
                        break;
                    case 2:
                        System.out.println("Nombre del deporte:");
                        sn.nextLine();
                        Deporte deporte = new Deporte(sn.nextLine());
                        System.out.println("Con balón? (S/N):");
                        if (sn.next().equalsIgnoreCase("S"))
                            deporte.setConBalon(true);
                        ministerio.adicionarDeporte(deporte);

                        break;
                    case 3:
                        sn.nextLine();
                        System.out.println("Nombre del deportista:");
                        String n = sn.nextLine();
                        System.out.println("Edad del deportista:");
                        int e = sn.nextInt();
                        ministerio.adicionarDeportista(new Deportista(n, e));
                        break;
                    case 4:
                        for (int i = 0; i < ministerio.getListado().size(); i++) {
                            Deporte d = ministerio.getListado().get(i);
                            if (d.isConBalon())
                                System.out.printf("Deporte #" + (i + 1) + ": " + d.getNombre() + "\n");
                        }
                        break;
                    case 5:
                        System.out.println("El promedio de edad de los deportistas es: " + ministerio.promedioEdad());
                        break;
                    case 6:
                        Especialista especialista1 = ministerio.getEspecialista();
                        if (especialista1 != null) {
                            System.out.println("Nombre del especialista: " + especialista1.getNombre());
                            System.out.println("Años de experiencia del especialista: " + especialista1.getAnnosExp());
                        }
                        break;
                    case 7:
                        for (Deportista d : ministerio.getDeportistas())
                            System.out.println("Nombre: " + d.getNombre() + ", edad: " + d.getEdad());
                        break;
                    case 0:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 7");
                }
                System.out.println("--------------");
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un número");
                sn.next();
            }
        }

    }

}