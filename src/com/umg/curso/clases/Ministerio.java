package com.umg.curso.clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ore on 28/06/17.
 */
public class Ministerio {

    private List<Deportista> deportistas;
    private List<Deporte> listado;
    private Especialista especialista;

    public Ministerio() {
        listado = new ArrayList<>();
        deportistas = new ArrayList<>();
    }

    public List<Deporte> getListado() {
        return listado;
    }

    public void setListado(List<Deporte> listado) {
        this.listado = listado;
    }

    public Especialista getEspecialista() {
        return especialista;
    }

    public void setEspecialista(Especialista especialista) {
        this.especialista = especialista;
    }

    public List<Deportista> getDeportistas() {
        return deportistas;
    }

    public void adicionarDeportista(Deportista d){
        if(d.getEdad() > 60)
            deportistas.add(0, d);
        else
            deportistas.add(d);
    }

    public float promedioEdad(){
        int suma = 0;
        for (Deportista d : deportistas){
            suma += d.getEdad();
        }
        return  suma/deportistas.size();
    }


    public void adicionarDeporte(Deporte d) {
        listado.add(d);
    }
}
